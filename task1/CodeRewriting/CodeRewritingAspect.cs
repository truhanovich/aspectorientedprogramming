﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PostSharp.Aspects;

namespace task1.CodeRewriting
{
    [Serializable]
    public class CodeRewritingAspect : OnMethodBoundaryAspect
    {
        public StringBuilder Result { get; set; }

        public override void OnEntry(MethodExecutionArgs args)
        {
            Result.Append(DateTime.Now.ToString("d"));
            Result.Append("; " + args.Method.Name);

            args.FlowBehavior = FlowBehavior.Default;
        }

        public override void OnSuccess(MethodExecutionArgs args)
        {
            Result.Append(string.Join(";", args.Arguments.Select(o => $"{nameof(o)}:{o.ToString()}").ToArray()));
            Result.Append(args.ReturnValue);
        }
    }
}
