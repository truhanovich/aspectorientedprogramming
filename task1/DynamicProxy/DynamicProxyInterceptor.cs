﻿using Castle.DynamicProxy;
using System;
using System.Linq;
using System.Text;

namespace task1.DynamicProxy
{
    public class DynamicProxyInterceptor : IInterceptor
    {
        public StringBuilder Result { get; set; }

        public DynamicProxyInterceptor()
        {
            Result = new StringBuilder();
        }
        public void Intercept(IInvocation invocation)
        {
            Result.Append(DateTime.Now.ToString("d"));
            Result.Append("; " + invocation.Method.Name);
            Result.Append(string.Join(";", invocation.Arguments.Select(o => $"{nameof(o)}:{o.ToString()}").ToArray()));
            invocation.Proceed();

            Result.Append(invocation.ReturnValue);
        }
    }
}
