﻿using System.Configuration;
using task1.ConfigManager;
using Topshelf;

namespace task1
{
    class Program
    {
        static void Main()
        {
            var folders = (TargetFoldersConfigSection)ConfigurationManager.GetSection("TargetFolders");

            HostFactory.Run(
                hostConf => hostConf.Service<FileService>(
                    s =>
                    {
                        s.ConstructUsing(() => new FileService(folders));
                        s.WhenStarted(serv => serv.Start());
                        s.WhenStopped(serv => serv.Stop());
                    }
                    ));
        }
    }
}
