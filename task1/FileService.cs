﻿using Castle.DynamicProxy;
using System.Configuration;
using System.IO;
using System.Threading;
using task1.BL;
using task1.ConfigManager;
using task1.DynamicProxy;

namespace task1
{
    class FileService
    {
        private readonly FileSystemWatcher[] _watcherList;
        private readonly ConfigurationElement[] _array;
        private readonly Thread _workThread;
        private readonly ManualResetEvent _stopWorkEvent;
        private readonly AutoResetEvent _newFileEvent;
        
        public FileService(TargetFoldersConfigSection folders)
        {
            _array = new ConfigurationElement[folders.FolderItems.Count];
            _watcherList = new FileSystemWatcher[folders.FolderItems.Count];
            folders.FolderItems.CopyTo(_array, 0);

            for (var i = 0; i < _array.Length; i++)
            {
                var folder = (FolderElement) _array[i];
                if (!Directory.Exists(folder.Path))
                {
                    Directory.CreateDirectory(folder.Path);
                }
                _watcherList[i] = new FileSystemWatcher(folder.Path);
                _watcherList[i].Created += Watcher_Created;
            }

            _stopWorkEvent = new ManualResetEvent(false);
            _newFileEvent = new AutoResetEvent(false);

            var generator = new ProxyGenerator();
            IFileWorker _fileWoker = generator.CreateInterfaceProxyWithTarget<IFileWorker>(new FileWorker(_array, _stopWorkEvent, _newFileEvent), 
                new DynamicProxyInterceptor());

            _workThread = new Thread(_fileWoker.WorkProcedure);
        }

        private void Watcher_Created(object sender, FileSystemEventArgs e)
        {
            _newFileEvent.Set();
        }

        public void Start()
        {
            _workThread.Start();
            foreach (var watcher in _watcherList)
            {
                watcher.EnableRaisingEvents = true;
            }
        }

        public void Stop()
        {
            foreach (var watcher in _watcherList)
            {
                watcher.EnableRaisingEvents = false;
            }
            _stopWorkEvent.Set();
            _workThread.Join();
        }
    }
}
