﻿using Ninject.Modules;
using task1.BL;

namespace task1.IoC
{
    class CommonModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IFileWorker>().To<FileWorker>();
        }
    }
}
